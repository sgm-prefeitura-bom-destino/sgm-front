import { SessionInternalService } from "./session.internal.service";
import { SessionService } from "./session.service";

const headers = new Headers({ 'content-type': 'application/json' });
export class ApiService {

  static async doLogin(user, password, internal = false) {
    SessionInternalService.logout();
    SessionService.logout();

    if (internal) {
      const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/internal/login`, {
        method: "POST",
        headers,
        credentials: "include",
        body: JSON.stringify({ matricula: user, password })
      }).then(a => a.json());

      if (response.statusCode) {
        throw new Error("Usuário ou senha inválidos");
      }
      SessionInternalService.setSession(response);

    } else {
      const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/login`, {
        method: "POST",
        headers,
        credentials: "include",
        body: JSON.stringify({ email: user, password })
      }).then(a => a.json());

      if (response.statusCode) {
        throw new Error("Usuário ou senha inválidos");
      }
      
      SessionService.setSession(response);
    }
  }

  static async register(user) {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/register`, {
      method: "POST",
      headers,
      credentials: "include",
      body: JSON.stringify(user)
    }).then(a => a.json());

    if (response.statusCode) {
      throw new Error("Campos inválidos ou usuário já existente");
    }
    return response;
  }

  static async fetchIptu(year) {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/tributacao/iptu?year=${year}`, {
      method: "GET",
      headers,
      credentials: "include",
    }).then(a => a.json());

    if (response.statusCode) {
      throw new Error("Falha ao realizar solicitacao");
    }
    return response;
  }

  static async fetchItr(year) {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/tributacao/iptu?year=${year}`, {
      method: "GET",
      headers,
      credentials: "include",
    }).then(a => a.json());

    if (response.statusCode) {
      throw new Error("Falha ao realizar solicitacao");
    }
    return response;
  }

  static async updateUserData(user) {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/cidadao`, {
      method: "PUT",
      headers,
      credentials: "include",
      body: JSON.stringify(user)
    }).then(a => a.json())

    if (response.statusCode) {
      throw new Error("Usuário ou senha inválidos");
    }

    return response;
  }

  static async fetchDoctor(postoId, date) {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/sasci/doctor?date=${date}&postoId=${postoId}`, {
      method: "GET",
      headers,
      credentials: "include",
    }).then(a => a.json());

    if (response.statusCode) {
      throw new Error("Falha ao realizar solicitacao");
    }
    return response;
  }

  static async saveAppointment(consulta) {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/sasci/appointment`, {
      method: "POST",
      headers,
      credentials: "include",
      body: JSON.stringify(consulta)
    })

    if (!response.ok) {
      throw new Error("Falha ao realizar solicitacao");
    }

    return response;
  }

  static async fetchAppointments() {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/sasci/appointment`, {
      method: "GET",
      headers,
      credentials: "include",
    }).then(a => a.json());

    if (response.statusCode) {
      throw new Error("Falha ao realizar solicitacao");
    }
    return response;
  }

  static async rescheduleAppointment(appointmentId, newDate) {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/sasci/appointment/reschedule`, {
      method: "PUT",
      headers,
      credentials: "include",
    })

    if (!response.ok) {
      throw new Error("Falha ao realizar solicitacao");
    }

    return response;
  }

  static async cancelAppointment(appointmentId) {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/sasci/appointment/cancel`, {
      method: "PUT",
      headers,
      credentials: "include",
    })

    if (!response.ok) {
      throw new Error("Falha ao realizar solicitacao");
    }

    return response;
  }
  
  static async cancelAppointmentInternal(appointmentId) {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/internal/sasci/appointment/cancel`, {
      method: "PUT",
      headers,
      credentials: "include",
    })

    if (!response.ok) {
      throw new Error("Falha ao realizar solicitacao");
    }

    return response;
  }

  static async approveAppointment(appointmentId) {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/internal/sasci/appointment/approve`, {
      method: "PUT",
      headers,
      credentials: "include",
    });

    if (!response.ok) {
      throw new Error("Falha ao realizar solicitacao");
    }

    return response;
  }

  static async fetchScheduledAppointments() {
    const response = await fetch(`${process.env.REACT_APP_API_ENDPOINT}/internal/sasci/appointment`, {
      method: "GET",
      headers,
      credentials: "include",
    }).then(a => a.json());

    if (response.statusCode) {
      throw new Error("Falha ao realizar solicitacao");
    }
    return response;
  }

}