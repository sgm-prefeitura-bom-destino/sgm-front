export class SessionServiceClass {

  getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
  }
  
  isLogged() {
    return this.getCookie("session") != null && this.getSession() != null;
  }

  getSession() {
    return JSON.parse(localStorage.getItem("user"));
  }

  setSession(newUser) {
    localStorage.setItem("user", JSON.stringify(newUser));
  }

  logout() {
    localStorage.removeItem("user");
    document.cookie = 'session=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    document.cookie = 'sessionInternal=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

}

export const SessionService = new SessionServiceClass();