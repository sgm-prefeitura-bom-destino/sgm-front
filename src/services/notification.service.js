import { toast } from "react-toastify";

export class NotificationService {

  static info(message) {
    toast.info(message);
  }
  
  static success(message) {
    toast.success(message);
  }
  
  static error(message) {
    toast.error(message);
  }
  static warn(message) {
    toast.warn(message);
  }

}