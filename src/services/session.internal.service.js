import { SessionServiceClass } from "./session.service";

export class SessionInternalServiceClass extends SessionServiceClass {

  isLogged() {
    return super.getCookie("sessionInternal") != null && super.getSession() != null;
  }

}

export const SessionInternalService = new SessionInternalServiceClass()