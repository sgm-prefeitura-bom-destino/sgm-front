import { version } from '../../../package.json';
import './index.less'

export const Author = () => {
  return (
    <footer className="author">
      <span>Desenvolvido por <a target="_blank" rel="noreferrer" href="https://www.linkedin.com/in/jvictoralves/">José Victor</a> - {new Date().getFullYear()} - v{version}</span><br/>
      <span>Pós Graduação em Arquitetura de Software Distribuído - PUC Minas</span>
    </footer>
  )
}