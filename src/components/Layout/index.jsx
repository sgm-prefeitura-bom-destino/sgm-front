import { Main } from './Main';
import { Header } from './Header';
import { Author } from '../Author';
import './index.less';
import { LayoutProvider } from './layout.context';

export const Layout = ({ children }) => {
  return (
    <LayoutProvider>
      <div className="layout">
        <Header />
        <Main>
          {children}
        </Main>
        <Author />
      </div>
    </LayoutProvider>
  )
}