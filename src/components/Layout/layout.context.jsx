import { createContext, useState } from "react";

export const LayoutContext = createContext({});

export const LayoutProvider = (props) => {
  const [openMenu, setOpenMenu] = useState(false);

  return (
    <LayoutContext.Provider value={{ openMenu, setOpenMenu }}>
      {props.children}
    </LayoutContext.Provider>
  )

}