import { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { NotificationService } from '../../../services/notification.service';
import { SessionService } from '../../../services/session.service';
import { LayoutContext } from '../layout.context';
import classnames from 'classnames';
import './index.less';

export const Header = () => {
  const history = useHistory();
  const { openMenu, setOpenMenu } = useContext(LayoutContext);
  
  const logout = (e) => {
    e.preventDefault();
    SessionService.logout();
    history.replace("/login");
    NotificationService.success("Você saiu do sistema com sucesso!");
  }

  return (
    <header>
      <div className="toggle-menu">
        <a href="void(0)" onClick={(e) => { e.preventDefault(); setOpenMenu(!openMenu); }}>
          <i className={classnames("icon large", {bars: !openMenu, close: openMenu})} />
        </a>
      </div>
      <h1 className="logo">
        <a href="/">SGM - Prefeitura de Bom Destino</a>
      </h1>
      <div className="right-menu-items">
        <a href="void(0)" onClick={logout}><i className="log out icon" /> Sair</a>
      </div>

    </header>
  );
}