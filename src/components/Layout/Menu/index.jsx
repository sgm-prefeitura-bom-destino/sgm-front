import classnames from 'classnames';
import { useContext } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { SessionService } from '../../../services/session.service';
import { LayoutContext } from '../layout.context';
import './index.less';

export const Menu = () => {
  const user = SessionService.getSession();
  const { openMenu } = useContext(LayoutContext);

  return (
    <aside className={classnames({ open: openMenu })}>
      <nav className="menu-items">
        <ul>
          <MenuItem icon="users" label="Dados do cidadão" href="/cidadao" />
          <MenuItem icon="doctor" label="Minha Saúde" href="/minha-saude" />
          <MenuItem icon="building outline" label="Imposto Predial e Territorial Urbano (IPTU)" href="/iptu" />
          <MenuItem icon="warehouse" label="Imposto Territorial Rural (ITR)" href="/itr" />
          <MenuItem icon="legal" label="Meus Processos" href="/meus-processos" disabled />
          <MenuItem icon="student" label="Educação" href="/educacao" disabled />
          <MenuItem icon="headphones" label="Ouvidoria" href="/ouvidoria" disabled />
        </ul>
      </nav>
      <nav className="user-info">
        <ul>
          <li><i className="icon user" /> <div className="user-name">{user?.name}</div>
            {/* <div className="user-type">({user.userType})</div> */}
          </li>
        </ul>
      </nav>
    </aside>
  )
}

const MenuItem = ({ icon, label, href, disabled = false }) => {
  const history = useHistory();
  const { pathname } = useLocation();
  const { setOpenMenu } = useContext(LayoutContext);

  const onClick = (e) => {
    e.preventDefault();
    if (!disabled) {
      setOpenMenu(false);
      history.push(href);
    }
  }
  return (
    <li className={classnames({ active: pathname.startsWith(href), disabled })} onClick={onClick}>
      <a href="void(0)">
        <i className={`icon ${icon}`} /> {label}
      </a>
      <i className="icon angle right" />
    </li>
  )
}