import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { SessionService } from '../../services/session.service';

export const PrivateRoute = ({ children, redirectTo = "/login", checkIfLoggedMethod = () => SessionService.isLogged(),  ...rest }) => {
  return (
    <Route {...rest} render={({ location }) =>
      checkIfLoggedMethod() ? (children) : (<Redirect to={{ pathname: redirectTo, state: { from: location } }} />)
    } />
  );
}