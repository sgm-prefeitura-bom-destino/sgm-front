import classnames from 'classnames';
import { useContext } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { SessionInternalService } from '../../../services/session.internal.service';
import { LayoutContext } from '../../Layout/layout.context';
import './index.less';

export const Menu = () => {
  const user = SessionInternalService.getSession();
  const { openMenu } = useContext(LayoutContext);

  return (
    <aside className={classnames({ open: openMenu })}>
      <nav className="menu-items">
        <ul>
          <MenuItem icon="doctor" label="Consultas agendadas" href="/internal/consultas-agendadas" />
        </ul>
      </nav>
      <nav className="user-info">
        <ul>
          <li><i className="icon user"/> <div className="user-name">{user?.name}</div> 
          </li>
        </ul>
      </nav>
    </aside>
  )
}

const MenuItem = ({ icon, label, href, disabled = false }) => {
  const history = useHistory();
  const { pathname } = useLocation();
  const { setOpenMenu } = useContext(LayoutContext);

  const onClick = (e) => {
    e.preventDefault();
    if (!disabled) {
      setOpenMenu(false);
      history.push(href);
    }
  }
  return (
    <li className={classnames({ active : pathname.startsWith(href), disabled })} onClick={onClick}>
      <a href="void(0)">
        <i className={`icon ${icon}`} /> {label}
      </a>
      <i className="icon angle right" />
    </li>
  )
}