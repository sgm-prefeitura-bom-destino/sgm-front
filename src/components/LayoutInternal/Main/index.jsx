import { Menu } from '../Menu';
import './index.less';

export const Main = ({ children }) => {
  return (
  <main>
    <Menu />
    <div className="content">
      {children}
    </div>
  </main>
  );
}