import classNames from 'classnames';
import { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { NotificationService } from '../../../services/notification.service';
import { SessionInternalService } from '../../../services/session.internal.service';
import { LayoutContext } from '../../Layout/layout.context';
import './index.less';

export const Header = () => {
  const history = useHistory();
  const { openMenu, setOpenMenu } = useContext(LayoutContext);

  const logout = (e) => {
    e.preventDefault();
    SessionInternalService.logout();
    history.replace("/internal/login");
    NotificationService.success("Você saiu do sistema com sucesso!");
  }

  return (
    <header>
      <div className="toggle-menu">
        <a href="void(0)" onClick={(e) => { e.preventDefault(); setOpenMenu(!openMenu); }}>
          <i className={classNames("icon large", {bars: !openMenu, close: openMenu})} />
        </a>
      </div>
      <h1 className="logo">
        <a href="/">SGM - Prefeitura de Bom Destino</a>
      </h1>
      <div className="right-menu-items">
        <a href="void(0)" onClick={logout}><i className="log out icon"/> Sair</a>
      </div>

    </header>
  );
}