import { Author } from '../Author';
import { LayoutProvider } from '../Layout/layout.context';
import { Header } from './Header';
import './index.less';
import { Main } from './Main';

export const LayoutInternal = ({ children }) => {
  return (
    <LayoutProvider>
      <div className="layout internal">
        <Header />
        <Main>
          {children}
        </Main>
        <Author />
      </div>
    </LayoutProvider>
  )
}