import { BrowserRouter, Route, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import ExternalPage from "./pages/external";
import InternalPages from "./pages/internal/index.jsx";
import LoginPage from "./pages/login/index.jsx";
import RegistrationPage from "./pages/registration/index.jsx";

const App = () => {
  return (
    <>
      <ToastContainer />
      <BrowserRouter basename="/">

        <Switch>

          <Route path="/login" component={LoginPage} />
          <Route path="/registration" component={RegistrationPage} />
          <Route path="/internal" component={InternalPages} />
          <Route path="/" component={ExternalPage} />

        </Switch>
      </BrowserRouter>
    </>
  )
}

export default App;