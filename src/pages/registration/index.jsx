import { Button, Card, Container, Form, Header } from "semantic-ui-react";
import MaskedInput from 'react-text-mask'
import { CPF_MASK } from "../../utils/masks";
import './index.less'
import { useHistory } from "react-router-dom";
import { Author } from "../../components/Author";
import { useState } from "react";
import { ApiService } from "../../services/api.service";
import { NotificationService } from "../../services/notification.service";

const RegistrationPage = () => {
  const [client, setClient] = useState({ name: "", cpf: "", address: "", email: "", password: "", });
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const onChange = (e) => {
    const newClient = { ...client, [e.target.name]: e.target.value };
    setClient(newClient);
  }

  const onSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      await ApiService.register(client);
      setLoading(false);
      NotificationService.success("✅ Cadastro realizado com sucesso!")
      history.push("/login");
    } catch (e) {
      setLoading(false);
      NotificationService.success(e.message);
    }

  }

  return (
    <Container className={"login registration"}>
      <div>
        <Card>
          <Card.Content>
            <Card.Header textAlign="center">Sistema de Gestão Integrada Municipal (SGM)</Card.Header>
            <Card.Meta textAlign="center">Prefeitura de Bom Destino</Card.Meta>
          </Card.Content>
          <Card.Content>
            <Form loading={loading} onSubmit={onSubmit}>
              <Header textAlign="center" size="tiny">Cadastro de acesso ao portal do cidadão</Header>
              <Form.Field required>
                <label>Nome completo</label>
                <input placeholder='Nome completo' required name="name" onChange={onChange} value={client.name} />
              </Form.Field>
              <Form.Field required>
                <label>CPF</label>
                <MaskedInput guide={false} minLength="14" mask={CPF_MASK} placeholder="Informe seu CPF" required name="cpf" onChange={onChange} value={client.cpf} />
              </Form.Field>
              <Form.Field required>
                <label>Endereço</label>
                <input placeholder='Endereço' required name="address" onChange={onChange} value={client.address} />
              </Form.Field>
              <Form.Field required>
                <label>E-mail</label>
                <input type="email" placeholder='E-mail' required name="email" onChange={onChange} value={client.email} />
              </Form.Field>
              <Form.Field required>
                <label>Senha</label>
                <input placeholder='Senha' minLength={6} type="password" required name="password" onChange={onChange} value={client.password} />
              </Form.Field>
              <Button secondary type='submit'>Cadastrar</Button>
              <Button secondary basic onClick={e => {
                e.preventDefault(); history.push('/login');
              }}>Cancelar</Button>
            </Form>
          </Card.Content>
        </Card>
      </div>
      <Author />
    </Container>
  );
}


export default RegistrationPage;