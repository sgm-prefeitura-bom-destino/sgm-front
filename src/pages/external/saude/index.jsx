import { Route, Switch, useHistory, useRouteMatch } from "react-router-dom"
import { Menu } from "semantic-ui-react"
import { AgendarConsulta } from "./agendar-consulta";
import { MinhasConsultas } from "./minhas-consultas";
import './index.less';

export const SaudePage = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route path={`${path}/agendar-consulta`} component={AgendarConsulta} />
      <Route path={`${path}/minhas-consultas`} component={MinhasConsultas} />
      <Route path={`${path}`} component={SaudeMenu} />
    </Switch>
  )

}

const SaudeMenu = () => {
  const history = useHistory();
  return (
    <div className="saude-menu">
      <Menu vertical>
        <Menu.Item name='agendar-consultas' onClick={() => history.push("/minha-saude/agendar-consulta")}>
          <i className="icon chevron right" /> Agendar Nova Consulta
      </Menu.Item>
        <Menu.Item name='minhas-consultas' onClick={() => history.push("/minha-saude/minhas-consultas")}>
          <i className="icon chevron right" /> Minhas Consultas
      </Menu.Item>
      </Menu>
    </div>
  )
}