import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import MaskedInput from "react-text-mask";
import { Button, Card, Dimmer, Dropdown, Form, Grid, List, Loader } from "semantic-ui-react";
import { ApiService } from "../../../../services/api.service";
import { NotificationService } from "../../../../services/notification.service";
import { DateFormatter } from "../../../../utils/formatter";
import { SUS_MASK } from "../../../../utils/masks";
import { Postos } from "../postos";


export const AgendarConsulta = () => {

  const [loading, setLoading] = useState(false);
  const [doctor, setDoctor] = useState(null);
  const [consulta, setConsulta] = useState({ posto: options[0].value });
  const history = useHistory();

  const onChange = (e) => {
    const newConsulta = { ...consulta, [e.target.name]: e.target.value };
    setConsulta(newConsulta);
  }

  const onSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    await ApiService.saveAppointment(consulta, doctor);
    setLoading(false);
    NotificationService.success(
      <>Solicitação de consulta realizada com sucesso!<br />Aguarde confirmação em 'Minhas Consultas'</>
    );
    history.push("/minha-saude");
  }

  const fetchDoctor = async (posto, date) => {
    setLoading(true);
    const doctor = await ApiService.fetchDoctor(posto, date);
    setDoctor(doctor);
    setLoading(false);
  }

  useEffect(() => {
    if (consulta.posto && consulta.date) {
      fetchDoctor(consulta.posto, consulta.date);
    }
  }, [consulta.posto, consulta.date])

  return (
    <Card className="cardContent">
      <Card.Content>
        <Card.Header>Agendamento de nova consulta</Card.Header>
        <Card.Meta>Agende uma consulta em um dos postos de saúde da cidade</Card.Meta>
      </Card.Content>

      {loading && (
        <Card.Content>
          <Dimmer active inverted>
            <Loader inverted>Carregando...</Loader>
          </Dimmer>
        </Card.Content>
      )}

      {!loading && (
        <Card.Content>
          <Form onSubmit={onSubmit} >
            <Grid stackable columns={1}>
              <Grid.Column>
                <Form.Field>
                  <label>Selecione um posto de atendimento</label>
                  <Dropdown placeholder="Selecione um posto de atendimento" value={consulta.posto} selection options={options}
                    onChange={(e, d) => onChange({ target: { name: "posto", value: d.value } })} />
                </Form.Field>
              </Grid.Column>

              <Grid.Column>
                <Form.Field>
                  <label>Selecione uma data para atendimento</label>
                  <input type="date" name="date" onChange={onChange} required value={consulta.date ?? ""} min={DateFormatter(new Date())} />
                </Form.Field>
              </Grid.Column>

              {doctor && (
                <Grid.Column>
                  <List>
                    <List.Item>
                      <List.Header>Médico que realizará seu atendimento</List.Header>
                      {doctor.name} (CRM {doctor.crm})
                    </List.Item>
                  </List>
                </Grid.Column>
              )}

              <Grid.Column>
                <Form.Field>
                  <label>Carteirinha do SUS</label>
                  <MaskedInput guide={false} minLength="18" mask={SUS_MASK} placeholder="Informe o número da sua carteirinha do SUS" required name="sus_number" onChange={onChange} value={consulta.sus_number ?? ""} />
                </Form.Field>
              </Grid.Column>

              <Grid.Column>
                <Button primary><i className="icon save" /> Salvar</Button>
                <Button onClick={e => { e.preventDefault(); history.push("/minha-saude") }}><i className="icon cancel" /> Cancelar</Button>
              </Grid.Column>
            </Grid>
          </Form>
        </Card.Content>
      )}
    </Card>
  )

}

const options = Postos.map((p, key) => (
  { key, text: p.name, value: p.id }
));