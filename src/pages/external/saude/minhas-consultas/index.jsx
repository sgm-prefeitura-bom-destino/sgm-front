import { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router";
import { Button, Card, Dimmer, Label, Loader, Popup, Table } from "semantic-ui-react";
import { ApiService } from "../../../../services/api.service";
import { renderDate } from "../../../../utils/date";
import { Postos } from "../postos";
import { RescheduleModalProvider, RescheduleModalContext } from "./reschedule-modal/reschedule-modal.context";
import { CancelModalContext, CancelModalProvider } from "./cancel-modal/cancel-modal.context";
import "./index.less"

const MinhasConsultasContent = () => {

  const history = useHistory();
  const { callback : rescheduleCallback } = useContext(RescheduleModalContext);
  const { callback : cancelCallback } = useContext(CancelModalContext);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  const fetchAppointments = async () => {
    setLoading(true);
    const data = await ApiService.fetchAppointments();
    setData(data);
    setLoading(false);
  }

  useEffect(() => fetchAppointments(), [rescheduleCallback, cancelCallback]);


  return (
    <Card className="cardContent">
      <Card.Content>
        <Card.Header>Minhas Consultas</Card.Header>
        <Card.Meta>Gerencie seu histórico de consultas</Card.Meta>
      </Card.Content>

      {loading && (
        <Card.Content>
          <Dimmer active inverted>
            <Loader inverted>Carregando...</Loader>
          </Dimmer>
        </Card.Content>
      )}

      {!loading && (
        <Card.Content>
          <Table basic="very">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Data da realização da consulta</Table.HeaderCell>
                <Table.HeaderCell>Posto de Saúde</Table.HeaderCell>
                <Table.HeaderCell>Médico</Table.HeaderCell>
                <Table.HeaderCell width="3">Status da Consulta</Table.HeaderCell>
                <Table.HeaderCell width="2"></Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {data.map((d, index) => (
                <Table.Row key={index}>
                  <Table.Cell>{renderDate(d.date)}</Table.Cell>
                  <Table.Cell>{Postos.find(p => p.id === d.posto)?.name}</Table.Cell>
                  <Table.Cell>{d.doctor.name} - (CRM {d.doctor.crm})</Table.Cell>
                  <Table.Cell width="3">
                    <AppointmentState consulta={d} />
                  </Table.Cell>
                  <Table.Cell width="2">
                    <Actions consulta={d} />
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>

          <Button onClick={() => history.push("/minha-saude")}><i className="icon arrow left" /> Voltar</Button>

        </Card.Content>
      )}
    </Card>
  )

}


const AppointmentState = ({ consulta }) => {

  if (consulta.status === "WAITING_CONFIRMATION") {
    return (
      <Popup
        content='Aguardando confirmação do seu agendamento, você será notificado por email após confirmação'
        position="right center" trigger={<Label size="mini" color='yellow'><i className="icon clock" /> Aguardando Confirmação</Label>}
      />
    )
  }

  if (consulta.status === "DONE") {
    return (
      <Label size="mini" color='green'><i className="icon check" /> Realizada</Label>
    )
  }

  if (consulta.status === "CONFIRMED") {
    return (
      <Popup
        content='Sua consulta foi confirmada e acontecerá na data prevista.'
        position="right center" trigger={<Label size="mini" color='teal'><i className="icon check" /> Confirmada</Label>}
      />
    )
  }

  if (consulta.status === "CANCELED") {
    return (
      <Popup
        content='Sua consulta foi cancelada.'
        position="right center" trigger={<Label size="mini" color='red'><i className="icon cancel" /> Cancelada</Label>}
      />
    )
  }

  return null;

}

const Actions = ({ consulta }) => {
  const { showModal : showRescheduleModal } = useContext(RescheduleModalContext);
  const { showModal : showCancelModal } = useContext(CancelModalContext);

  if (["WAITING_CONFIRMATION", "CONFIRMED"].includes(consulta.status)) {
    return (
      <div className="btn_actions">
        <Popup content='Reagendar' trigger={<Button size="small" basic color="yellow" icon='clock' onClick={() => showRescheduleModal(true, consulta)} />} />
        <Popup content='Cancelar consulta' trigger={<Button size="small" basic color="red" icon='cancel' onClick={() => showCancelModal(true, consulta)} />} />
      </div>
    )
  }

  return null;
}

export const MinhasConsultas = () => {
  return (
    <RescheduleModalProvider>
      <CancelModalProvider>
        <MinhasConsultasContent />
      </CancelModalProvider>
    </RescheduleModalProvider>
  )
}