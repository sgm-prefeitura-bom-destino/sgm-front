import { createContext, useState } from "react";
import { RescheduleModal } from "./reschedule-modal";

export const RescheduleModalContext = createContext({});

export const RescheduleModalProvider = (props) => {
  const [open, setShow] = useState(false);
  const [consulta, setConsulta] = useState(null);
  const [callback, setCallback] = useState({});

  const triggerCallback = () => {
    setCallback({});
  }

  const showModal = (open, consulta = null) => {
    setShow(open);
    setConsulta(consulta);
  }

  return (
    <RescheduleModalContext.Provider value={{ open, showModal, callback }}>
      <RescheduleModal open={open} showModal={showModal} consulta={consulta} triggerCallback={triggerCallback} />
      {props.children}
    </RescheduleModalContext.Provider>
  )

}