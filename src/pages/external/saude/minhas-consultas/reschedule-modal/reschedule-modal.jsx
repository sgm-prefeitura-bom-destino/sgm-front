import { useEffect, useRef, useState } from "react";
import { Button, Form, Modal } from "semantic-ui-react";
import { ApiService } from "../../../../../services/api.service";
import { NotificationService } from "../../../../../services/notification.service";
import { isInvalidDate } from "../../../../../utils/date";
import { DateFormatter } from "../../../../../utils/formatter";

export const RescheduleModal = ({ open, showModal, consulta, triggerCallback }) => {

  const formRef = useRef();
  const [date, setDate] = useState(null);
  const [loading, setLoading] = useState(false);

  const onSubmit = async () => {
    setLoading(true);
    await ApiService.rescheduleAppointment();
    setLoading(false);
    showModal(false);
    NotificationService.success("Reagendado com sucesso!");
    triggerCallback();
  }

  useEffect(() => {
    setDate(consulta?.date);
  }, [consulta])

  return (
    <Modal onClose={() => showModal(false)} open={open} closeOnEscape={!loading} closeOnDimmerClick={!loading}>
      <Modal.Header>Informe a nova data da consulta</Modal.Header>
      <Modal.Content>

        <Modal.Description>
          <Form ref={formRef} onSubmit={onSubmit} loading={loading}>
            <Form.Field>
              <label>Selecione uma nova data para atendimento</label>
              <input type="date" value={date ?? ""} name="date" onChange={e => setDate(e.target.value)} required min={DateFormatter(new Date())} />
            </Form.Field>
          </Form>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button content="Cancelar" disabled={loading} icon="cancel" labelPosition="left" onClick={() => showModal(false)} />
        <Button content="Confirmar" disabled={isInvalidDate(date)} loading={loading} labelPosition='left' icon='save' onClick={() => formRef?.current.handleSubmit()} positive />
      </Modal.Actions>

    </Modal>
  )

}