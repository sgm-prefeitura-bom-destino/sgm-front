import { createContext, useState } from "react";
import { CancelModal } from "./cancel-modal";

export const CancelModalContext = createContext({});

export const CancelModalProvider = (props) => {
  const [open, setShow] = useState(false);
  const [consulta, setConsulta] = useState(null);
  const [callback, setRescheduleCallback] = useState({});

  const triggerCallback = () => {
    setRescheduleCallback({});
  }

  const showModal = (open, consulta = null) => {
    setShow(open);
    setConsulta(consulta);
  }

  return (
    <CancelModalContext.Provider value={{ open, showModal, callback }}>
      <CancelModal open={open} showModal={showModal} consulta={consulta} triggerCallback={triggerCallback} />
      {props.children}
    </CancelModalContext.Provider>
  )

}