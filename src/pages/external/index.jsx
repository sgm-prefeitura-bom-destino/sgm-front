import { Route, Switch, useRouteMatch } from "react-router-dom";
import { Layout } from "../../components/Layout";
import { PrivateRoute } from "../../components/PrivateRoute";
import { CidadaoPage } from "./cidadao";
import { HomePage } from "./home";
import { IPTUPage } from "./iptu";
import { ITRPage } from "./itr";
import { SaudePage } from "./saude";

const ExternalPage = () => {
  const { path } = useRouteMatch();
  return (
    <PrivateRoute>
      <Layout>
        <Switch>
          <Route path={`${path}iptu`} component={IPTUPage} />
          <Route path={`${path}itr`} component={ITRPage} />
          <Route path={`${path}cidadao`} component={CidadaoPage} />
          <Route path={`${path}minha-saude`} component={SaudePage} />
          <Route path={path} exact component={HomePage} />
        </Switch>
      </Layout>
    </PrivateRoute>
  )
}

export default ExternalPage;