import { Label, Popup, Table } from "semantic-ui-react";
import boleto from '../../../../assets/boleto.png';
import certidaoNegativa from '../../../../assets/iptu_certidao_negativa.jpeg';
import './index.less';

const IptuTable = ({ data}) => {


  if (!data || data.length === 0) {
    return (
      <div className="iptu-table--no-data">
        <i className="icon exclamation triangle large" />
        Nenhum imóvel vinculado ao CPF para o ano selecionado
      </div>
    )
  }

  return (
    <>
      <div className="title">Lista de Imóveis vinculados ao seu CPF no período informado:</div>
      <Table basic="very">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Inscrição do Imóvel</Table.HeaderCell>
            <Table.HeaderCell>Endereço</Table.HeaderCell>
            <Table.HeaderCell>Status do IPTU</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {data.map((d, index) => (
            <Table.Row key={index}>
              <Table.Cell>{d.inscricao}</Table.Cell>
              <Table.Cell>{d.endereco}</Table.Cell>
              <Table.Cell>
                <PaymentStatus status={d.status} billId={d.billId} />
              </Table.Cell>
              <Table.Cell>
                <NoDebitCertified status={d.status} billId={d.billId} />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    </>
  )
}

const PaymentStatus = ({ status, billId }) => {
  let component = <Label color='teal'><i className="icon check" /> PAGO</Label>;
  if (status === "PENDING") {
    const onClick = (e) => window.open(`${boleto}?billId=${billId}`, "_blank");
    component = (
      <Popup
        content='Clique para gerar o boleto' position="right center"
        trigger={<Label onClick={onClick} as="a" color='yellow'><i className="icon clock" /> PENDENTE</Label>}
      />
    )
  }
  return component;
}

const NoDebitCertified = ({ status, billId }) => {
  if (status === "PENDING") {
    return null;
  }

  return (
    <a rel="noreferrer" href={`${certidaoNegativa}?billId=${billId}`} target="_blank">Emitir Nada Consta</a>
  )
}

export default IptuTable;