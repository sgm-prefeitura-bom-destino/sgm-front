import { useEffect, useState } from "react"
import { Button, Card, Dimmer, Dropdown, Form, Grid, Loader } from "semantic-ui-react"
import { ApiService } from "../../../services/api.service"
import './index.less'
import IptuTable from "./iptu-table"

export const IPTUPage = () => {

  const [year, setYear] = useState(currentYear);
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(null);
  const fetchIptu = async (year) => {
    setLoading(true);
    const data = await ApiService.fetchIptu(year);
    setData(data);
    setLoading(false);
  }

  useEffect(() => fetchIptu(year), [year]);

  return (
    <Card className="iptu">
      <Card.Content>
        <Card.Header>Consulta de IPTU</Card.Header>
        <Card.Meta>Verifique e corrija pendências do seu IPTU desse ano ou de anos anteriores</Card.Meta>
      </Card.Content>

      {loading && (
        <Card.Content>
          <Dimmer active inverted>
            <Loader inverted>Carregando...</Loader>
          </Dimmer>
        </Card.Content>
      )}

      {!loading && (
        <Card.Content>
          <Form onSubmit={() => fetchIptu(year)} >
            <Grid stackable columns={1}>
              <Grid.Column>
                <Form.Field>
                  <label>Selecione um ano</label>
                  <Dropdown placeholder="Selecione um ano" value={year} selection options={yearsOptions} onChange={(e, d) => {
                    setYear(d.value);
                  }} />
                </Form.Field>
              </Grid.Column>

              <Grid.Column largeScreen={6}>
                <Button fluid primary>Selecionar</Button>
              </Grid.Column>
            </Grid>
          </Form>

          <IptuTable data={data} />
        </Card.Content>
      )}
    </Card>
  )
}

const currentYear = new Date().getFullYear();
const yearsOptions = [
  { key: currentYear, text: currentYear, value: currentYear },
  { key: currentYear - 1, text: currentYear - 1, value: currentYear - 1 },
  { key: currentYear - 2, text: currentYear - 2, value: currentYear - 2 },
]