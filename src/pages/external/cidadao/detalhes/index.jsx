import { useState } from "react";
import { useHistory } from "react-router-dom";
import { Button, Card, Dimmer, Item, Loader } from "semantic-ui-react";
import { SessionService } from "../../../../services/session.service";

export const DetalhesCidadao = () => {

  const [loading] = useState(false);
  const history = useHistory();
  const user = SessionService.getSession();

  return (
    <Card className="cardContent">
      <Card.Content>
        <Card.Header>Dados do cidadão</Card.Header>
        <Card.Meta>Verifique e corrija divergências das informações do seu cadastro</Card.Meta>
      </Card.Content>

      {loading && (
        <Card.Content>
          <Dimmer active inverted>
            <Loader inverted>Carregando...</Loader>
          </Dimmer>
        </Card.Content>
      )}

      <Card.Content>

        <Item.Group>
          <Item>
            <Item.Content>
              <Item.Header>Nome</Item.Header>
              <Item.Meta>
                {user?.name}
              </Item.Meta>
            </Item.Content>
          </Item>
          <UserTypeItem user={user} />
          <Item>
            <Item.Content>
              <Item.Header>Endereço para correspondências</Item.Header>
              <Item.Meta>
                {user?.address}
              </Item.Meta>
            </Item.Content>
          </Item>

          <Item>
            <Item.Content>
              <Item.Header>E-mail</Item.Header>
              <Item.Meta>
                {user?.email}
              </Item.Meta>
            </Item.Content>
          </Item>

        </Item.Group>

        <div>
          <Button onClick={() => history.push("/cidadao/atualizar-dados")} primary><i className="icon edit" /> Atualizar minhas informações </Button>
        </div>

      </Card.Content>

    </Card>
  )
}

const UserTypeItem = ({ user }) => {
  return (
    <Item>
      <Item.Content>
        <Item.Header>CPF</Item.Header>
        <Item.Meta>Pessoa Física</Item.Meta>
        <Item.Meta>{user?.cpf}</Item.Meta>
      </Item.Content>
    </Item>
  )
}