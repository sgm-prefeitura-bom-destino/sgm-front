import { useState } from "react";
import { useHistory } from "react-router-dom";
import { Button, Card, Dimmer, Form, Loader } from "semantic-ui-react";
import { ApiService } from "../../../../services/api.service";
import { NotificationService } from "../../../../services/notification.service";
import { SessionService } from "../../../../services/session.service";

export const AtualizarDadosCidadao = () => {
  const user = SessionService.getSession();
  const [loading, setLoading] = useState(false);
  const [editedUser, setUser] = useState(user);
  const history = useHistory();

  const onChange = (e) => {
    const newUser = { ...editedUser, [e.target.name]: e.target.value };
    setUser(newUser);
  }

  const onSubmit = async e => {
    e.preventDefault();
    setLoading(true);

    try {
      const { password, ...userSessionData } = await ApiService.updateUserData(editedUser);
      SessionService.setSession(userSessionData);
      NotificationService.success("Dados atualizados com sucesso!");
      setLoading(false);
      history.push("/cidadao");
    } catch (error) {
      NotificationService.error(error.message);
      setLoading(false);
    }
  }

  return (
    <Card className="cardContent">
      <Card.Content>
        <Card.Header>Dados do cidadão</Card.Header>
        <Card.Meta>Verifique e corrija divergências das informações do seu cadastro</Card.Meta>
      </Card.Content>

      {loading && (
        <Card.Content>
          <Dimmer active inverted>
            <Loader inverted>Carregando...</Loader>
          </Dimmer>
        </Card.Content>
      )}

      <Card.Content>

        <Form onSubmit={onSubmit}>
          <Form.Field>
            <label>Nome</label>
            <input required placeholder='Nome' name="name" value={editedUser.name} onChange={onChange} />
          </Form.Field>

          <Form.Field>
            <label>CPF</label>
            <input disabled placeholder='CPF' name="cpf" value={editedUser.cpf} />
          </Form.Field>

          <Form.Field>
            <label>Endereço para correspondências</label>
            <input required placeholder='Endereço' name="address" value={editedUser.address} onChange={onChange} />
          </Form.Field>

          <Form.Field>
            <label>E-mail</label>
            <input disabled placeholder='E-mail' name="email" value={editedUser.email} />
          </Form.Field>
          <Form.Field>
            <label>Digite a sua senha atual</label>
            <input required type="password" placeholder='Senha' name="password" value={editedUser.password ?? ""} onChange={onChange} />
          </Form.Field>

          <Button primary type="submit"><i className="icon save" /> Salvar</Button>
          <Button onClick={e => { e.preventDefault(); history.push("/cidadao") }} ><i className="icon cancel" /> Cancelar</Button>

        </Form>

      </Card.Content>

    </Card>
  )
}