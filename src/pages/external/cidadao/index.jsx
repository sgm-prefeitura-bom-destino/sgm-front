import { Route, Switch, useRouteMatch } from "react-router-dom";
import { AtualizarDadosCidadao } from "./atualizar-dados";
import { DetalhesCidadao } from "./detalhes";

export const CidadaoPage = () => {
  const { path } = useRouteMatch();
  return (
    <Switch>
      <Route path={`${path}/atualizar-dados`} component={AtualizarDadosCidadao} />
      <Route path={`${path}`} component={DetalhesCidadao} />
    </Switch>
  )
}

