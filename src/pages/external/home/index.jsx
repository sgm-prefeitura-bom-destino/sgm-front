import { useEffect } from "react";
import { useHistory } from "react-router-dom";

export const HomePage = () => {
  const history = useHistory();
  useEffect(() => history.replace("/cidadao"), [history])
  return null;
  // return (
  //   `Olá ${user.name}, seja bem vindo ao portal de serviços ao cidadão`
  // )
}