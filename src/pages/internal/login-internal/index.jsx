import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import MaskedInput from 'react-text-mask';
import { Button, Card, Container, Form, Header } from 'semantic-ui-react';
import { Author } from '../../../components/Author';
import { ApiService } from '../../../services/api.service';
import { NotificationService } from '../../../services/notification.service';
import { MATRICULA_MASK } from '../../../utils/masks';
import './index.less';

const LoginInternalPage = () => {
  const [user, setUser] = useState({ matricula: "", password: "" });
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const onChange = (e) => {
    const newUser = { ...user, [e.target.name]: e.target.value };
    setUser(newUser);
  }

  const onSubmit = async (e) => {
    e.preventDefault();
    setLoading(true)
    try {
      await ApiService.doLogin(user.matricula, user.password, true);
      setLoading(false);
      history.push("/")
    } catch (e) {
      NotificationService.error("Usuário ou senha inválidos");
      setLoading(false);
    }

  }

  return (
    <Container className={"login internal"}>
      <div>
        <Card>
          <Card.Content>
            <Card.Header textAlign="center">Sistema de Gestão Integrada Municipal (SGM)</Card.Header>
            <Card.Meta textAlign="center">Prefeitura de Bom Destino</Card.Meta>
          </Card.Content>
          <Card.Content>
            <Form loading={loading} onSubmit={onSubmit}>
              <Header textAlign="center" size="tiny">Acesso de Funcionários Públicos</Header>
              <Form.Field>
                <label>Matricula</label>
                <MaskedInput guide={false} minLength="8" mask={MATRICULA_MASK} placeholder="Matricula" required name="matricula" onChange={onChange} value={user.matricula} />
              </Form.Field>
              <Form.Field>
                <label>Senha</label>
                <input required placeholder='Senha' type="password" name="password" value={user.password} onChange={onChange} />
              </Form.Field>
              <Button primary type='submit'>Entrar</Button>
            </Form>
          </Card.Content>
          <Card.Content>
            <a href="void(0)" onClick={e => { e.preventDefault(); history.push("/login") }}>Acesso ao portal do cidadão</a>
          </Card.Content>
        </Card>
      </div>

      <Author />
    </Container>
  );
}


export default LoginInternalPage;