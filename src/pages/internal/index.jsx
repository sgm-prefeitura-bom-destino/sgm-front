import { Redirect, Route, Switch, useRouteMatch } from "react-router-dom";
import { LayoutInternal } from "../../components/LayoutInternal";
import { PrivateRoute } from "../../components/PrivateRoute";
import { SessionInternalService } from "../../services/session.internal.service.js";
import { ConsultasAgendadas } from "./consultas-agendadas";
import LoginInternalPage from "./login-internal/index.jsx";

const InternalPages = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route path={`${path}/login`} component={LoginInternalPage} />
      <PrivateRoute redirectTo={`/internal/login`} checkIfLoggedMethod={() => SessionInternalService.isLogged()}>
        <LayoutInternal>
          <Switch>
            <Route path={`${path}/consultas-agendadas`} component={ConsultasAgendadas} />
            <Redirect to={`${path}/consultas-agendadas`} />
          </Switch>
        </LayoutInternal>
      </PrivateRoute>
    </Switch>
  )
}

export default InternalPages;