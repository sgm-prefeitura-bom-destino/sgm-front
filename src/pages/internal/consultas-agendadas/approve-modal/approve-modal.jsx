import { useState } from "react";
import { Button, Modal } from "semantic-ui-react";
import { ApiService } from "../../../../services/api.service";
import { NotificationService } from "../../../../services/notification.service";
import { renderDate } from "../../../../utils/date";

export const ApproveModal = ({ open, showModal, consulta, triggerCallback }) => {

  const [loading, setLoading] = useState(false);

  const onConfirm = async () => {
    setLoading(true);
    await ApiService.approveAppointment(consulta.id);
    setLoading(false);
    showModal(false);
    NotificationService.success(`Consulta do dia ${renderDate(consulta.date)} aprovada com sucesso!`);
    triggerCallback();
  }

  return (
    <Modal onClose={() => showModal(false)} open={open} closeOnEscape={!loading} closeOnDimmerClick={!loading}>
      <Modal.Header>Aprovação de consulta</Modal.Header>
      <Modal.Content>
        <p>Deseja realmente aprovar a consulta do dia {renderDate(consulta?.date)} do paciente {consulta?.paciente.name} - SUS {consulta?.paciente.sus_number}?</p>
      </Modal.Content>
      <Modal.Actions>
        <Button content="Cancelar" disabled={loading} icon="cancel" labelPosition="left" onClick={() => showModal(false)} />
        <Button content="Confirmar" loading={loading} labelPosition='left' icon='save' onClick={onConfirm} positive />
      </Modal.Actions>

    </Modal>
  )

}