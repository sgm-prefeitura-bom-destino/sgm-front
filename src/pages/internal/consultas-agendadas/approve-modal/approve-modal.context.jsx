import { createContext, useState } from "react";
import { ApproveModal } from "./approve-modal";

export const ApproveModalContext = createContext({});

export const ApproveModalProvider = (props) => {
  const [open, setShow] = useState(false);
  const [consulta, setConsulta] = useState(null);
  const [callback, setCallback] = useState({});

  const triggerCallback = () => {
    setCallback({});
  }

  const showModal = (open, consulta = null) => {
    setShow(open);
    setConsulta(consulta);
  }

  return (
    <ApproveModalContext.Provider value={{ open, showModal, callback }}>
      <ApproveModal open={open} showModal={showModal} consulta={consulta} triggerCallback={triggerCallback} />
      {props.children}
    </ApproveModalContext.Provider>
  )

}