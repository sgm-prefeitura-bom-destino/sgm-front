import { useContext, useEffect, useState } from "react";
import { Button, Card, Dimmer, Label, Loader, Popup, Table } from "semantic-ui-react";
import { ApiService } from "../../../services/api.service";
import { renderDate } from "../../../utils/date";
import { Postos } from "../../external/saude/postos";
import { ApproveModalContext, ApproveModalProvider } from "./approve-modal/approve-modal.context";
import { CancelModalContext, CancelModalProvider } from "./cancel-modal/cancel-modal.context";

const ConsultasAgendadasPage = () => {

  const { callback: callbackApprove } = useContext(ApproveModalContext);
  const { callback: callbackCancel } = useContext(CancelModalContext);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null); 

  const fetchAppointments = async () => {
    setLoading(true);
    const data = await ApiService.fetchScheduledAppointments();
    setData(data);
    setLoading(false);
  }

  useEffect(() => fetchAppointments(), [callbackApprove, callbackCancel]);

  return (
    <Card className="cardContent">
      <Card.Content>
        <Card.Header>Consultas Agendadas</Card.Header>
        <Card.Meta>Verifique e gerencie as consultas agendadas</Card.Meta>
      </Card.Content>

      {loading && (
        <Card.Content>
          <Dimmer active inverted>
            <Loader inverted>Carregando...</Loader>
          </Dimmer>
        </Card.Content>
      )}

      <Card.Content>

        <Table basic="very">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Data do agendamento</Table.HeaderCell>
              <Table.HeaderCell>Paciente</Table.HeaderCell>
              <Table.HeaderCell>Posto de Saúde</Table.HeaderCell>
              <Table.HeaderCell>Médico</Table.HeaderCell>
              <Table.HeaderCell width="3">Status da Consulta</Table.HeaderCell>
              <Table.HeaderCell width="2"></Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {data?.map((d, index) => (
              <Table.Row key={index}>
                <Table.Cell>{renderDate(d.date)}</Table.Cell>
                <Table.Cell>{d.paciente.name}<br />Nº SUS: {d.paciente.sus_number}</Table.Cell>
                <Table.Cell>{Postos.find(p => p.id === d.posto)?.name}</Table.Cell>
                <Table.Cell>{d.doctor.name} - (CRM {d.doctor.crm})</Table.Cell>
                <Table.Cell width="3">
                  <AppointmentState consulta={d} />
                </Table.Cell>
                <Table.Cell width="2">
                  <Actions consulta={d} />
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>

      </Card.Content>

    </Card>
  )
}

const AppointmentState = ({ consulta }) => {

  if (consulta.status === "WAITING_CONFIRMATION") {
    return (
      <Popup
        content='Aguardando confirmação do agendamento'
        position="right center" trigger={<Label size="mini" color='yellow'><i className="icon clock" /> Aguardando Confirmação</Label>}
      />
    )
  }

  if (consulta.status === "DONE") {
    return (
      <Label size="mini" color='green'><i className="icon check" /> Realizada</Label>
    )
  }

  if (consulta.status === "CONFIRMED") {
    return (
      <Popup
        content='Sua consulta foi confirmada e acontecerá na data prevista.'
        position="right center" trigger={<Label size="mini" color='teal'><i className="icon check" /> Confirmada</Label>}
      />
    )
  }

  if (consulta.status === "CANCELED") {
    return (
      <Popup
        content='Sua consulta foi cancelada.'
        position="right center" trigger={<Label size="mini" color='red'><i className="icon cancel" /> Cancelada</Label>}
      />
    )
  }

  return null;

}

const Actions = ({ consulta }) => {
  const { showModal: showApproveModal } = useContext(ApproveModalContext);
  const { showModal: showCancelModal } = useContext(CancelModalContext);

  if ("WAITING_CONFIRMATION" === consulta.status) {
    return (
      <div className="btn_actions">
        <Popup content='Confirmar consulta' trigger={<Button size="small" basic color="teal" icon='check' onClick={() => showApproveModal(true, consulta)} />} />
        <Popup content='Cancelar consulta' trigger={<Button size="small" basic color="red" icon='cancel' onClick={() => showCancelModal(true, consulta)} />} />
      </div>
    )
  }

  if (["CONFIRMED"].includes(consulta.status)) {
    return (
      <div className="btn_actions">
        <Popup content='Cancelar consulta' trigger={<Button size="small" basic color="red" icon='cancel' onClick={() => showCancelModal(true, consulta)} />} />
      </div>
    )
  }

  return null;
}

export const ConsultasAgendadas = () => {
  return (
    <ApproveModalProvider>
      <CancelModalProvider>
        <ConsultasAgendadasPage />
      </CancelModalProvider>
    </ApproveModalProvider>
  )
}