import { useState } from "react";
import { Button, Modal } from "semantic-ui-react";
import { ApiService } from "../../../../services/api.service";
import { NotificationService } from "../../../../services/notification.service";
import { renderDate } from "../../../../utils/date";

export const CancelModal = ({ open, showModal, consulta, triggerCallback }) => {

  const [loading, setLoading] = useState(false);

  const onConfirm = async () => {
    setLoading(true);
    await ApiService.cancelAppointmentInternal(consulta.id);
    setLoading(false);
    showModal(false);
    NotificationService.success(`Consulta do dia ${renderDate(consulta.date)} cancelada com sucesso!`);
    triggerCallback();
  }

  return (
    <Modal onClose={() => showModal(false)} open={open} closeOnEscape={!loading} closeOnDimmerClick={!loading}>
      <Modal.Header>Cancelamento de consulta</Modal.Header>
      <Modal.Content>
        <p>Deseja realmente cancelar a consulta do dia {renderDate(consulta?.date)}?</p>
      </Modal.Content>
      <Modal.Actions>
        <Button content="Cancelar" disabled={loading} icon="cancel" labelPosition="left" onClick={() => showModal(false)} />
        <Button content="Confirmar" loading={loading} labelPosition='left' icon='save' onClick={onConfirm} positive />
      </Modal.Actions>

    </Modal>
  )

}