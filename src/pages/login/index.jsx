import React, { useState } from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import { Button, Card, Container, Form, Header } from 'semantic-ui-react';
import { Author } from '../../components/Author';
import { ApiService } from '../../services/api.service';
import { NotificationService } from '../../services/notification.service';
import { SessionInternalService } from '../../services/session.internal.service';
import { SessionService } from '../../services/session.service';
import './index.less';

const LoginPage = () => {
  const [user, setUser] = useState({ email: "", password: "" });
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const onChange = (e) => {
    const newUser = { ...user, [e.target.name]: e.target.value };
    setUser(newUser);
  }

  const onSubmit = async (e) => {
    e.preventDefault();
    setLoading(true)
    try {
      await ApiService.doLogin(user.email, user.password);
      setLoading(false);
      history.push("/")
    } catch (e) {
      NotificationService.error("Usuário ou senha inválidos");
      setLoading(false);
    }

  }

  if (SessionService.isLogged()) {
    return <Redirect to="/" />
  }

  if (SessionInternalService.isLogged()) {
    return <Redirect to="/internal" />
  }

  return (
    <Container className="login">
      <div>
        <Card>
          <Card.Content>
            <Card.Header textAlign="center">Sistema de Gestão Integrada Municipal (SGM)</Card.Header>
            <Card.Meta textAlign="center">Prefeitura de Bom Destino</Card.Meta>
          </Card.Content>
          <Card.Content>
            <Form loading={loading} onSubmit={onSubmit}>
              <Header textAlign="center" size="tiny">Acesso ao Portal do Cidadão</Header>
              <Form.Field>
                <label>E-mail</label>
                <input required placeholder='E-mail' type="email" name="email" value={user.email} onChange={onChange} />
              </Form.Field>
              <Form.Field>
                <label>Senha</label>
                <input required placeholder='Senha' type="password" name="password" value={user.password} onChange={onChange} />
              </Form.Field>
              <Button secondary type='submit'>Entrar</Button>
              <Button secondary basic onClick={e => {
                e.preventDefault(); history.push('/registration')
              }}>Registrar-se</Button>
            </Form>
          </Card.Content>
          <Card.Content>
            <a href="void(0)" disabled={loading} onClick={e => { e.preventDefault(); history.push('/internal/login') }}>Acesso de funcionários públicos</a>
          </Card.Content>
        </Card>
      </div>
      <Author />
    </Container>
  );
}


export default LoginPage;