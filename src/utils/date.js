export const renderDate = (date) => {
  return new Date(`${date} 00:00:00`).toLocaleDateString();
}

export const isInvalidDate = (date) => {
  return isNaN(new Date(date).getDate());
}