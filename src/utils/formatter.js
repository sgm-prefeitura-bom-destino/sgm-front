const floatFormatter = new Intl.NumberFormat('pt-br', { minimumIntegerDigits: 1, minimumFractionDigits: 2, maximumFractionDigits: 2 });
const dateFormatter = new Intl.DateTimeFormat('sv-SE', { year: 'numeric', month: '2-digit', day: '2-digit'})

const generateFormatter = (formatterFn) => {
  return (value, defaultValue = "") => {
    if (value === null || value === undefined || value === "") {
      return defaultValue;
    }
    return formatterFn(value);
  }
}

export const FloatFormatter = generateFormatter(floatFormatter.format);
export const DateFormatter = generateFormatter(dateFormatter.format);