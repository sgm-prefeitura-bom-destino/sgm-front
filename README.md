

## Descrição

Front end do SGM.
Contém a implementação da interface dos novos modulos do sistema de gestão integrada muncipal da prefeitura de 
bom destino.
## Instalação

```bash
$ npm install
```

## Executar a aplicação

```bash
# development
$ npm run start

# production mode
$ npm run build
```

## Sobre

Desenvolvido por [José Victor](https://www.linkedin.com/in/jvictoralves/).

Pós Graduação em Arquitetura de Software Distribuído - PUC Minas.

