CONTEXT=${1:-vpsdime}

rm -r build
npm run build

docker context use $CONTEXT
docker-compose build
docker stack deploy --compose-file docker-compose.yml sgm-service
# docker-compose up --build -d
docker image prune -f
docker context use default